package com.example.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myfirstapp.activity_playground.JavaActivity;
import com.example.myfirstapp.fragment_playground.FragmentPlaygroundActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToActivityPlayground(View view) {

        Intent intent = new Intent(MainActivity.this, JavaActivity.class);
        startActivity(intent);

    }

    public void goToFragmentPlayground(View view) {

        Intent intent = new Intent(MainActivity.this, FragmentPlaygroundActivity.class);
        startActivity(intent);

    }
}
