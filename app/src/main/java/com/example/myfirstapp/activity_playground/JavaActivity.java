package com.example.myfirstapp.activity_playground;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.myfirstapp.MainActivity;
import com.example.myfirstapp.R;

public class JavaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java);
    }

    public void goToActivityKotlin(View view) {
        Intent intent = new Intent(JavaActivity.this, KotlinActivity.class);
        startActivity(intent);
    }
}
