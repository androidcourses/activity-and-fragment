package com.example.myfirstapp.activity_playground;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.myfirstapp.R;

public class KotlinActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kotlin);
    }

    public void goToActivityCPP(View view) {
        Intent intent = new Intent(KotlinActivity.this, CPPActivity.class);
        startActivity(intent);
    }

    public void goBack(View view) {
        finish();
    }
}
