package com.example.myfirstapp.fragment_playground.helpers;

public class Constants {

    public static final String FRAGMENT_TAG_THOR = "FRAGMENT_TAG_THOR";
    public static final String FRAGMENT_TAG_IRON_MAN = "FRAGMENT_TAG_IRON_MAN";
    public static final String FRAGMENT_TAG_CAPTAIN_AMERICA = "FRAGMENT_TAG_CAPTAIN_AMERICA";
    public static final String FRAGMENT_TAG_STAN_LEE = "FRAGMENT_TAG_STAN_LEE";

}
