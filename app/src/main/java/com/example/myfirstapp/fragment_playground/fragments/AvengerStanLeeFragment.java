package com.example.myfirstapp.fragment_playground.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.myfirstapp.R;
import com.example.myfirstapp.fragment_playground.interfaces.OnFragmentInteractionListener;

public class AvengerStanLeeFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public static AvengerStanLeeFragment newInstance() {
        AvengerStanLeeFragment fragment = new AvengerStanLeeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_avenger_stan_lee, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView thor = view.findViewById(R.id.imv_stan_lee);
        thor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAvengerPressed();
            }
        });
    }

    public void onAvengerPressed() {
        if (mListener != null) {
            mListener.onAvengerSaid(getString(R.string.message_avenger_stan_lee));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
