package com.example.myfirstapp.fragment_playground;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import com.example.myfirstapp.fragment_playground.fragments.AvengerCaptainAmericaFragment;
import com.example.myfirstapp.fragment_playground.fragments.AvengerIronManFragment;
import com.example.myfirstapp.fragment_playground.fragments.AvengerStanLeeFragment;
import com.example.myfirstapp.fragment_playground.fragments.AvengerThorFragment;
import com.example.myfirstapp.fragment_playground.helpers.Constants;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Toast;

import com.example.myfirstapp.R;
import com.example.myfirstapp.fragment_playground.interfaces.OnFragmentInteractionListener;

public class FragmentPlaygroundActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnFragmentInteractionListener {

    FragmentManager mFragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_playground);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        mFragmentManager = this.getSupportFragmentManager();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.fragment_playground, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_iron_man:
                replaceFragment(Constants.FRAGMENT_TAG_IRON_MAN);
                break;
            case R.id.nav_captain:
                replaceFragment(Constants.FRAGMENT_TAG_CAPTAIN_AMERICA);
                break;
            case R.id.nav_thor:
                replaceFragment(Constants.FRAGMENT_TAG_THOR);
                break;
            case R.id.nav_stan_lee:
                replaceFragment(Constants.FRAGMENT_TAG_STAN_LEE);
                break;
        }

        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(item.getTitle());
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void replaceFragment(String TAG) {
        Fragment fragment;
        switch (TAG) {
            case Constants.FRAGMENT_TAG_THOR: {
                fragment = AvengerThorFragment.newInstance("nothing", "really");
                break;
            }
            case Constants.FRAGMENT_TAG_IRON_MAN: {
                fragment = AvengerIronManFragment.newInstance();
                break;
            }
            case Constants.FRAGMENT_TAG_CAPTAIN_AMERICA: {
                fragment = AvengerCaptainAmericaFragment.newInstance();
                break;
            }
            case Constants.FRAGMENT_TAG_STAN_LEE: {
                fragment = AvengerStanLeeFragment.newInstance();
                break;
            }
            default: return;
        }

        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.fragments_container,
                fragment,
                TAG);
//        mFragmentTransaction.addToBackStack(TAG);
        mFragmentTransaction.commit();
    }

    @Override
    public void onAvengerSaid(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
